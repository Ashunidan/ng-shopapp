package com.filipwiniarz.shopapp;

import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;

import java.util.Date;

public class Mocks {

    public static ShoppingList activeList1() {
        ShoppingList list = new ShoppingList();
        list.setName("MockActiveList1");
        list.setCreateDate(new Date(1494166069));
        list.setState(ShoppingListState.ACTIVE);
        return list;
    }

    public static ShoppingList archivedList1() {
        ShoppingList list = new ShoppingList();
        list.setName("Buy something today");
        list.setCreateDate(new Date(120000000));
        list.setState(ShoppingListState.ARCHIVED);
        return list;
    }
}
