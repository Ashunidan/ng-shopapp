package com.filipwiniarz.shopapp.views.activity;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.filipwiniarz.shopapp.CustomActions;
import com.filipwiniarz.shopapp.CustomMatchers;
import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.DbManager;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.filipwiniarz.shopapp.CustomActions.clickChildViewWithId;
import static com.filipwiniarz.shopapp.CustomMatchers.itemAtPosition;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ShoppingListsActivityTest {

    @Rule
    public ActivityTestRule<ShoppingListsActivity> mActivityTestRule = new ActivityTestRule<>(ShoppingListsActivity.class);

    @Before
    public void setUp() {
        DbManager dbManager = new DbManager(mActivityTestRule.getActivity());
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingListTable.TABLE_NAME);
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingItemTable.TABLE_NAME);
        dbManager.onCreate(dbManager.getWritableDatabase());
    }

    @Test
    public void shoppingListsActivityTest_createDeleteRestoreDeleteList() {
        //add list
        ViewInteraction floatingActionButton = onView(allOf(withId(R.id.add_shopping_list_fab), isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction textInputEditText = onView(allOf(withId(R.id.title_edit_text), isDisplayed()));
        textInputEditText.perform(replaceText("TestList1"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(allOf(withId(R.id.dialog_save_button), isDisplayed()));
        appCompatButton.perform(click());

        //check name
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler)))
                .check(matches(itemAtPosition(0, hasDescendant(withText("TestList1")))));

        //delete
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler))).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.delete_button)));

        //check snackbar text and undo
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.shopping_list_deleted)))
                .check(matches(isDisplayed()));
        onView(withText(R.string.general_undo)).perform(click());

        //check name
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler)))
                .check(matches(itemAtPosition(0, hasDescendant(withText("TestList1")))));

        //delete
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler))).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.delete_button)));
    }

    @Test
    public void shoppingListsActivityTest_createArchiveUndoAndUnarchiveCheck() throws InterruptedException {
        //add item
        ViewInteraction floatingActionButton = onView(allOf(withId(R.id.add_shopping_list_fab), isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction textInputEditText = onView(allOf(withId(R.id.title_edit_text), isDisplayed()));
        textInputEditText.perform(replaceText("TestList1"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(allOf(withId(R.id.dialog_save_button), isDisplayed()));
        appCompatButton.perform(click());

        //check name
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler)))
                .check(matches(itemAtPosition(0, hasDescendant(withText("TestList1")))));

        //archive
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler))).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.archive_button)));


        //check snackbar text and undo
        Thread.sleep(2000);
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.shopping_list_archived)))
                .check(matches(isDisplayed()));
        onView(withText(R.string.general_undo)).perform(click());

        //check name
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler)))
                .check(matches(itemAtPosition(0, hasDescendant(withText("TestList1")))));

        //archive
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler))).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.archive_button)));

        //swipe to archived lists
        onView(withId(R.id.view_pager)).perform(swipeLeft());


        //unarchive
        Thread.sleep(1000);
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler))).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.archive_button)));


        //check snackbar text and undo
        Thread.sleep(2000);
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.shopping_list_unarchived)))
                .check(matches(isDisplayed()));
        onView(withText(R.string.general_undo)).perform(click());

        //delete
        onView(allOf(isCompletelyDisplayed(), withId(R.id.shopping_list_recycler))).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.delete_button)));
    }
}
