package com.filipwiniarz.shopapp.views.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.filipwiniarz.shopapp.Mocks;
import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.DbManager;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.filipwiniarz.shopapp.CustomActions.clickChildViewWithId;
import static com.filipwiniarz.shopapp.CustomMatchers.itemAtPosition;
import static com.filipwiniarz.shopapp.CustomMatchers.withToolbarTitle;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ShoppingDetailsActivityTest {

    @Rule
    public ActivityTestRule<ShoppingDetailsActivity> mActivityTestRule = new ActivityTestRule<>(ShoppingDetailsActivity.class, false, false);

    @Test
    public void ShoppingDetailsActivityTest_activeListTitleEditAndProductAddingDeleting() throws InterruptedException {
        startActivityWithActiveList();
        prepareDatabase();

        //check title
        onView(withId(R.id.toolbar_layout)).check(matches(withToolbarTitle(Mocks.activeList1().getName())));

        //edit title
        onView(allOf(withId(R.id.edit_title_fab), isDisplayed())).perform(click());
        onView(allOf(withId(R.id.title_edit_text), isDisplayed())).perform(replaceText("Modified"), closeSoftKeyboard());
        onView(allOf(withId(R.id.dialog_save_button), isDisplayed())).perform(click());

        //check modified title
        onView(withId(R.id.toolbar_layout)).check(matches(withToolbarTitle("Modified")));

        //check empty list title
        Thread.sleep(1000);
        onView(allOf(withId(R.id.empty_list_label), isDisplayed())).check(matches(withText(R.string.shopping_items_empty)));

        //add product
        onView(allOf(withId(R.id.add_item_fab), isDisplayed())).perform(click());
        onView(allOf(withId(R.id.name_edit_text), isDisplayed())).perform(replaceText("Chicken legs"), closeSoftKeyboard());
        onView(allOf(withId(R.id.amount_edit_text), isDisplayed())).perform(replaceText("1.5 kg"), closeSoftKeyboard());
        onView(allOf(withId(R.id.dialog_save_button), isDisplayed())).perform(click());

        //add product
        onView(allOf(withId(R.id.add_item_fab), isDisplayed())).perform(click());
        onView(allOf(withId(R.id.name_edit_text), isDisplayed())).perform(replaceText("Milk"), closeSoftKeyboard());
        onView(allOf(withId(R.id.amount_edit_text), isDisplayed())).perform(replaceText("3 l"), closeSoftKeyboard());
        onView(allOf(withId(R.id.dialog_save_button), isDisplayed())).perform(click());

        //check product names on list
        ViewInteraction productsRecycler = onView(allOf(isCompletelyDisplayed(), withId(R.id.items_recycler))).check(matches(itemAtPosition(0, hasDescendant(withText("Milk")))));
        productsRecycler.check(matches(itemAtPosition(0, hasDescendant(withText("Milk")))));
        productsRecycler.check(matches(itemAtPosition(0, hasDescendant(withText("3 l")))));
        productsRecycler.check(matches(itemAtPosition(1, hasDescendant(withText("Chicken legs")))));
        productsRecycler.check(matches(itemAtPosition(1, hasDescendant(withText("1.5 kg")))));

        //delete product
        productsRecycler.perform(RecyclerViewActions.actionOnItemAtPosition(0, clickChildViewWithId(R.id.delete_button)));

        //check snackbar text and undo
        Thread.sleep(2000);
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.product_deleted))).check(matches(isDisplayed()));
        onView(withText(R.string.general_undo)).perform(click());

        //check restored product names
        productsRecycler.check(matches(itemAtPosition(0, hasDescendant(withText("Milk")))));
        productsRecycler.check(matches(itemAtPosition(0, hasDescendant(withText("3 l")))));
    }

    @Test
    public void ShoppingDetailsActivityTest_archivedListButtonsDisplaying() {
        startActivityWithArchivedList();
        prepareDatabase();

        //check title
        onView(withId(R.id.toolbar_layout)).check(matches(withToolbarTitle(Mocks.archivedList1().getName())));

        //check edit title fab visibility
        onView(withId(R.id.edit_title_fab)).check(matches(not(isDisplayed())));

        //check add product fab visibility
        onView(withId(R.id.add_item_fab)).check(matches(not(isDisplayed())));
    }

    private void startActivityWithActiveList() {
        ShoppingList arg = Mocks.activeList1();
        arg.setId(1L);
        Intent startIntent = new Intent();
        startIntent.putExtra(ShoppingDetailsActivity.DETAILS_OBJECT_ARG, arg);
        mActivityTestRule.launchActivity(startIntent);
    }

    private void startActivityWithArchivedList() {
        ShoppingList arg = Mocks.archivedList1();
        arg.setId(1L);
        Intent startIntent = new Intent();
        startIntent.putExtra(ShoppingDetailsActivity.DETAILS_OBJECT_ARG, arg);
        mActivityTestRule.launchActivity(startIntent);
    }

    private void prepareDatabase() {
        DbManager dbManager = new DbManager(mActivityTestRule.getActivity());
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingListTable.TABLE_NAME);
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingItemTable.TABLE_NAME);
        dbManager.onCreate(dbManager.getWritableDatabase());
        dbManager.getWritableDatabase().execSQL("INSERT INTO " + DbContract.ShoppingListTable.TABLE_NAME + " ("+DbContract.ShoppingListTable.COLUMN_NAME+") " +
                "VALUES(\""+Mocks.activeList1().getName()+"\")");
    }
}