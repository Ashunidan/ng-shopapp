package com.filipwiniarz.shopapp.presenters;

import android.util.Log;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.database.helpers.DbQueryManager;
import com.filipwiniarz.shopapp.views.interfaces.ShoppingListsView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ShoppingListsPresenter extends BasePresenter<ShoppingListsView> {

    private DbQueryManager queryManager = DbQueryManager.createInstance();

    public void fetchShoppingLists(ShoppingListState stateCriterion) {
        getCompositeDisposable().add(queryManager.getShoppingListDbService().fetchShoppingLists(getMvpContext(), stateCriterion)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(shoppingLists -> {
                    if(shoppingLists.isEmpty()) {
                        getMvpView().displayEmptyListMessage();
                    } else {
                        getMvpView().displayShoppingLists(shoppingLists);
                    }
                }, throwable -> Log.e(getClass().getSimpleName(), "Error fetching shopping lists!", throwable)));
    }

    public void deleteShoppingList(ShoppingList list) {

        if(list.getState() != ShoppingListState.ACTIVE && list.getState() != ShoppingListState.ARCHIVED) {
            throw new IllegalStateException("Trying to delete already deleted list!");
        }

        list.setState(ShoppingListState.ACTIVE == list.getState() ? ShoppingListState.DELETED_ACTIVE : ShoppingListState.DELETED_ARCHIVED);
        getCompositeDisposable().add(queryManager.getShoppingListDbService().updateShoppingList(getMvpContext(), list)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(() -> getMvpView().handleShoppingListDelete(list),
                throwable -> Log.e(getClass().getSimpleName(), "Error deleting shopping list", throwable)));
    }

    public void archiveShoppingList(ShoppingList list) {
        list.setState(ShoppingListState.ARCHIVED);
        getCompositeDisposable().add(queryManager.getShoppingListDbService().updateShoppingList(getMvpContext(), list)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getMvpView().handleShoppingListArchive(list),
                        throwable -> Log.e(getClass().getSimpleName(), "Error archiving shopping list", throwable)));
    }

    public void unarchiveShoppingList(ShoppingList list) {
        list.setState(ShoppingListState.ACTIVE);
        getCompositeDisposable().add(queryManager.getShoppingListDbService().updateShoppingList(getMvpContext(), list)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getMvpView().handleShoppingListUnarchive(list),
                        throwable -> Log.e(getClass().getSimpleName(), "Error unarchiving shopping list", throwable)));
    }
}
