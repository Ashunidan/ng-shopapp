package com.filipwiniarz.shopapp.presenters;

import android.util.Log;

import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingItemState;
import com.filipwiniarz.shopapp.database.helpers.DbQueryManager;
import com.filipwiniarz.shopapp.views.interfaces.ShoppingDetailsView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ShoppingDetailsPresenter extends BasePresenter<ShoppingDetailsView> {

    private DbQueryManager queryManager = DbQueryManager.createInstance();

    public void fetchShoppingItemsForList(Long shoppingListId) {
        getCompositeDisposable().add(queryManager.getShoppingItemDbService().fetchShoppingItemsForList(getMvpContext(), shoppingListId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(items -> {
                if(items.isEmpty()) {
                    getMvpView().displayEmptyList();
                } else {
                    getMvpView().displayShoppingItems(items);
                }
            }, throwable -> Log.e(getClass().getSimpleName(), "Error fetching shopping items!", throwable)));
    }

    public void updateListTitle(ShoppingList list, String newTitle) {
        list.setName(newTitle);
        getCompositeDisposable().add(queryManager.getShoppingListDbService().updateShoppingList(getMvpContext(), list)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getMvpView().handleTitleEdit(list),
                        throwable -> Log.e(getClass().getSimpleName(), "Error updating shopping list title", throwable)));
    }

    public void addListItem(ShoppingItem item) {
        getCompositeDisposable().add(queryManager.getShoppingItemDbService().insertShoppingItem(getMvpContext(), item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getMvpView()::handleNewShoppingItem,
                        throwable -> Log.e(ShoppingListsActivityPresenter.class.getSimpleName(), "Error adding shopping list", throwable)));
    }

    public void deleteListItem(ShoppingItem item) {
        getCompositeDisposable().add(queryManager.getShoppingItemDbService().deleteShoppingItem(getMvpContext(), item.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getMvpView().handleRemoveShoppingItem(item),
                        throwable -> Log.e(getClass().getSimpleName(), "Error deleting shopping item", throwable)));
    }

    public void markListItemAsBought(ShoppingItem item, boolean marked) {
        item.setState(marked ? ShoppingItemState.BOUGHT : ShoppingItemState.ACTIVE);
        getCompositeDisposable().add(queryManager.getShoppingItemDbService().updateShoppingItem(getMvpContext(), item)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe());
    }
}
