package com.filipwiniarz.shopapp.presenters;

import android.content.Context;

import com.filipwiniarz.shopapp.views.MvpView;
import com.filipwiniarz.shopapp.views.activity.MvpActivity;
import com.filipwiniarz.shopapp.views.fragment.MvpFragment;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<T extends MvpView> implements MvpPresenter<T> {

    private T mvpView;
    private CompositeDisposable compositeDisposable;

    public T getMvpView() {
        return mvpView;
    }

    @Override
    public void attachView(T view) {
        this.mvpView = view;
    }

    @Override
    public void detachView() {
        if(compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }

    public CompositeDisposable getCompositeDisposable() {
        if(compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    public Context getMvpContext() {
        Context result = null;

        if(mvpView instanceof MvpFragment) {
            result = ((MvpFragment)mvpView).getContext();
        } else if(mvpView instanceof MvpActivity) {
            result = (Context) mvpView;
        }

        return result;
    }
}
