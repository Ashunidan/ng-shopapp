package com.filipwiniarz.shopapp.presenters;


import com.filipwiniarz.shopapp.views.MvpView;

public interface MvpPresenter<T extends MvpView> {

    void attachView(T view);
    void detachView();
}
