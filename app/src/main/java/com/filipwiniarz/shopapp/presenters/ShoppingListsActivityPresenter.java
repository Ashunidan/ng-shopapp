package com.filipwiniarz.shopapp.presenters;

import android.util.Log;

import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.database.helpers.DbQueryManager;
import com.filipwiniarz.shopapp.views.interfaces.ShoppingListsActivityView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ShoppingListsActivityPresenter extends BasePresenter<ShoppingListsActivityView> {

    private DbQueryManager queryManager = DbQueryManager.createInstance();

    public void saveNewShoppingList(ShoppingList shoppingList) {
        getCompositeDisposable().add(queryManager.getShoppingListDbService().insertShoppingList(getMvpContext(), shoppingList)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                    getMvpView()::handleAddShoppingListSuccess,
                    throwable -> {
                        Log.e(ShoppingListsActivityPresenter.class.getSimpleName(), "Error adding shopping list", throwable);
                        getMvpView().handleAddShoppingListFailure();
                    }
            ));
    }

    public void restoreShoppingList(ShoppingList shoppingList) {

        if(shoppingList.getState() != ShoppingListState.DELETED_ACTIVE && shoppingList.getState() != ShoppingListState.DELETED_ARCHIVED) {
            throw new IllegalStateException("Trying to restore already restored list!");
        }

        shoppingList.setState(shoppingList.getState() == ShoppingListState.DELETED_ACTIVE ? ShoppingListState.ACTIVE : ShoppingListState.ARCHIVED);
        getCompositeDisposable().add(queryManager.getShoppingListDbService().updateShoppingList(getMvpContext(), shoppingList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        getMvpView()::handleAddShoppingListSuccess,
                        throwable -> {
                            Log.e(ShoppingListsActivityPresenter.class.getSimpleName(), "Error adding shopping list", throwable);
                            getMvpView().handleAddShoppingListFailure();
                        }
                ));
    }
}
