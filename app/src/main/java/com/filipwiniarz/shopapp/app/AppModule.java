package com.filipwiniarz.shopapp.app;

import android.app.Application;

import com.filipwiniarz.shopapp.database.DbManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Singleton
    @Provides
    Application provideApp() {
        return app;
    }

    @Singleton
    @Provides
    DbManager provideDatabase() {
        return new DbManager(app);
    }

}
