package com.filipwiniarz.shopapp.app;

import android.app.Application;
import android.content.Context;

import com.filipwiniarz.shopapp.database.DbManager;
import javax.inject.Inject;

public class App extends Application {

    @Inject DbManager database;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        appComponent.inject(this);
    }

    public DbManager getDatabase() {
        return database;
    }

    public static AppComponent getAppComponent(Context context) {
        return ((App)context.getApplicationContext()).appComponent;
    }
}
