package com.filipwiniarz.shopapp.views.activity;

import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.presenters.ShoppingListsActivityPresenter;
import com.filipwiniarz.shopapp.views.dialogs.AddShoppingListDialog;
import com.filipwiniarz.shopapp.views.fragment.ShoppingListsFragment;
import com.filipwiniarz.shopapp.views.interfaces.ShoppingListsActivityView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ShoppingListsActivity extends MvpActivity<ShoppingListsActivityPresenter> implements ShoppingListsActivityView {

    private FragmentsPageAdapter mPageAdapter;

    @BindView(R.id.view_pager) ViewPager mViewPager;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tab_layout) TabLayout tabLayout;
    @BindView(R.id.add_shopping_list_fab) FloatingActionButton addShoppingListFab;

    @Override
    public ShoppingListsActivityPresenter buildPresenter() {
        return new ShoppingListsActivityPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        mPageAdapter = new FragmentsPageAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mPageAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_shopping_lists;
    }


    @OnClick(R.id.add_shopping_list_fab)
    public void onFabClick() {
        displayAddShoppingListView();
    }

    @Override
    public void displayAddShoppingListView() {
        AddShoppingListDialog dialog = new AddShoppingListDialog(this, getPresenter());
        dialog.show();
    }

    @Override
    public void handleAddShoppingListSuccess() {
        Snackbar.make(addShoppingListFab, R.string.add_shopping_list_success, Snackbar.LENGTH_SHORT).show();
        updateAllLists();
    }

    @Override
    public void handleAddShoppingListFailure() {
        Snackbar.make(addShoppingListFab, R.string.add_shopping_list_failure, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void updateAllLists() {
        for(ShoppingListsFragment fragment : mPageAdapter.fragments) {
            fragment.updateShoppingLists();
        }
    }

    public class FragmentsPageAdapter extends FragmentStatePagerAdapter {

        List<ShoppingListsFragment> fragments;

        FragmentsPageAdapter(FragmentManager fm) {
            super(fm);
            this.fragments = new ArrayList<>();
            this.fragments.add(ShoppingListsFragment.newInstance(ShoppingListState.ACTIVE));
            this.fragments.add(ShoppingListsFragment.newInstance(ShoppingListState.ARCHIVED));
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
