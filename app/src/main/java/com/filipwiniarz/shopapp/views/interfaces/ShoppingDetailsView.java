package com.filipwiniarz.shopapp.views.interfaces;

import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.views.MvpView;

import java.util.List;

public interface ShoppingDetailsView extends MvpView {

    void displayShoppingItems(List<ShoppingItem> shoppingItems);
    void displayEmptyList();
    void handleNewShoppingItem();
    void handleRemoveShoppingItem(ShoppingItem item);
    void handleTitleEdit(ShoppingList list);
    void handleArchivedMode();
}
