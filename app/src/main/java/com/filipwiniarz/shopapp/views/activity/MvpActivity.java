package com.filipwiniarz.shopapp.views.activity;

import android.os.Bundle;

import com.filipwiniarz.shopapp.presenters.MvpPresenter;
import com.filipwiniarz.shopapp.views.MvpView;

public abstract class MvpActivity<T extends MvpPresenter> extends BaseActivity implements MvpView {

    private T presenter;

    public T getPresenter() {
        return presenter;
    }

    public abstract T buildPresenter();

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attachView(this);
    }
}
