package com.filipwiniarz.shopapp.views.recycler.viewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.enums.ShoppingItemState;
import com.filipwiniarz.shopapp.views.listeners.ItemDeleteListener;
import com.filipwiniarz.shopapp.views.listeners.ItemMarkListener;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ShoppingItemViewHolder extends BaseViewHolder<ShoppingItem> {

    private final ItemDeleteListener<ShoppingItem> deleteListener;
    private final ItemMarkListener markListener;
    private final boolean archived;

    @BindView(R.id.root) View rootView;
    @BindView(R.id.bought_checkbox) CheckBox boughtCheckbox;
    @BindView(R.id.shopping_item_title) TextView itemTitle;
    @BindView(R.id.shopping_item_amount) TextView itemAmount;
    @BindView(R.id.delete_button) ImageView deleteButton;
    @BindView(R.id.text_container) View textContainer;

    public ShoppingItemViewHolder(ViewGroup parent, ItemDeleteListener<ShoppingItem> deleteListener, ItemMarkListener markListener, boolean archived) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_shopping_item, parent, false));
        this.deleteListener = deleteListener;
        this.markListener = markListener;
        this.context = parent.getContext();
        this.archived = archived;
    }

    @Override
    public void bindItem(ShoppingItem item) {
        this.item = item;
        rootView.setBackgroundColor(item.getState() == ShoppingItemState.BOUGHT ? context.getResources().getColor(R.color.lightGray) : context.getResources().getColor(R.color.white));
        boughtCheckbox.setChecked(item.getState() == ShoppingItemState.BOUGHT);
        itemTitle.setText(item.getName());
        itemAmount.setText(item.getAmount());
        deleteButton.setVisibility(archived ? View.GONE : View.VISIBLE);
        boughtCheckbox.setVisibility(archived ? View.GONE : View.VISIBLE);
    }

    @OnCheckedChanged(R.id.bought_checkbox)
    public void onChecked(boolean isChecked) {
        rootView.setBackgroundColor(isChecked ? context.getResources().getColor(R.color.lightGray) : context.getResources().getColor(R.color.white));
        item.setState(isChecked ? ShoppingItemState.BOUGHT : ShoppingItemState.ACTIVE);

        if(markListener != null) {
            markListener.onMark(item, isChecked);
        }
    }

    @OnClick(R.id.text_container)
    public void onClickTickCheckbox() {
        if(!archived) {
            boughtCheckbox.toggle();
            onChecked(boughtCheckbox.isChecked());
        }
    }

    @OnClick(R.id.delete_button)
    public void onDeleteClick() {
        if(deleteListener != null) {
            deleteListener.onItemDelete(item);
        }
    }
}
