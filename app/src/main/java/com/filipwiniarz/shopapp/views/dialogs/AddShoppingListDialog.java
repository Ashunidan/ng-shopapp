package com.filipwiniarz.shopapp.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.presenters.ShoppingListsActivityPresenter;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class AddShoppingListDialog extends Dialog {

    private ShoppingListsActivityPresenter presenter;

    @BindView(R.id.dialog_title) TextView dialogTitle;
    @BindView(R.id.title_edit_text) EditText listTitleField;
    @BindView(R.id.dialog_save_button) Button saveButton;
    @BindView(R.id.dialog_cancel_button) Button cancelButton;

    public AddShoppingListDialog(Context context, @NonNull ShoppingListsActivityPresenter presenter) {
        super(context);
        setContentView(R.layout.dialog_add_shopping_list);
        this.presenter = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        saveButton.setEnabled(false);
        dialogTitle.setText(R.string.new_shopping_list);
    }

    @OnClick(R.id.dialog_cancel_button)
    public void cancelClick() {
        dismiss();
    }

    @OnClick(R.id.dialog_save_button)
    public void saveClick() {
        String title = listTitleField.getText().toString();
        ShoppingList newShoppingList = new ShoppingList();
        newShoppingList.setName(title);
        newShoppingList.setState(ShoppingListState.ACTIVE);
        newShoppingList.setCreateDate(new Date());
        dismiss();
        presenter.saveNewShoppingList(newShoppingList);
    }

    @OnTextChanged(R.id.title_edit_text)
    public void onTextChanged(CharSequence charSequence) {
        saveButton.setEnabled(charSequence.length() != 0);
    }
}
