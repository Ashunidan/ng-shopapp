package com.filipwiniarz.shopapp.views.recycler.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.List;

public abstract class BaseRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    List<T> adapterItems;

    public int getItemListPosition(T item) {
        return adapterItems.indexOf(item);
    }

    @Override
    public int getItemCount() {
        return adapterItems.size();
    }

    public void setAndDisplayItems(List<T> items) {
        adapterItems.clear();
        adapterItems.addAll(items);
        notifyDataSetChanged();
    }

    public int deleteListItem(T item) {
        int position = getItemListPosition(item);
        adapterItems.remove(position);
        notifyItemRemoved(position);
        return position;
    }

    public void insertListItem(T item, int position) {
        adapterItems.add(position, item);
        notifyItemInserted(position);
    }
}
