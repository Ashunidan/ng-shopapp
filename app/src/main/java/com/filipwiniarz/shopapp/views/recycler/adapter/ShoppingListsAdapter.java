package com.filipwiniarz.shopapp.views.recycler.adapter;

import android.view.ViewGroup;

import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.views.listeners.ItemArchiveListener;
import com.filipwiniarz.shopapp.views.listeners.ItemDeleteListener;
import com.filipwiniarz.shopapp.views.listeners.ItemUnarchiveListener;
import com.filipwiniarz.shopapp.views.listeners.OpenDetailsListener;
import com.filipwiniarz.shopapp.views.recycler.viewholder.ShoppingListViewHolder;

import java.util.ArrayList;

public class ShoppingListsAdapter extends BaseRecyclerAdapter<ShoppingList, ShoppingListViewHolder> {

    private final ItemDeleteListener<ShoppingList> deleteListener;
    private final ItemArchiveListener<ShoppingList> archiveListener;
    private final ItemUnarchiveListener<ShoppingList> unarchiveListener;
    private final OpenDetailsListener<ShoppingList> openDetailsListener;

    public ShoppingListsAdapter(ItemDeleteListener<ShoppingList> deleteListener,
                                ItemArchiveListener<ShoppingList> archiveListener,
                                ItemUnarchiveListener<ShoppingList> unarchiveListener,
                                OpenDetailsListener<ShoppingList> openDetailsListener) {
        this.deleteListener = deleteListener;
        this.archiveListener = archiveListener;
        this.unarchiveListener = unarchiveListener;
        this.openDetailsListener = openDetailsListener;
        this.adapterItems = new ArrayList<>();
    }

    @Override
    public ShoppingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShoppingListViewHolder(parent, deleteListener, archiveListener, unarchiveListener, openDetailsListener);
    }

    @Override
    public void onBindViewHolder(ShoppingListViewHolder holder, int position) {
        holder.bindItem(adapterItems.get(position));
    }
}
