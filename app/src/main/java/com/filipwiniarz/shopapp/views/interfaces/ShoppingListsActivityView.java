package com.filipwiniarz.shopapp.views.interfaces;

import com.filipwiniarz.shopapp.views.MvpView;

public interface ShoppingListsActivityView extends MvpView{

    void displayAddShoppingListView();
    void handleAddShoppingListSuccess();
    void handleAddShoppingListFailure();
    void updateAllLists();
}
