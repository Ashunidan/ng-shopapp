package com.filipwiniarz.shopapp.views.listeners;

import com.filipwiniarz.shopapp.database.entities.ShoppingItem;

public interface ItemMarkListener {

    void onMark(ShoppingItem item, boolean marked);
}
