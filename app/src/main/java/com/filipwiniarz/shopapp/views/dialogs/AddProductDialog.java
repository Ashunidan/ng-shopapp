package com.filipwiniarz.shopapp.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingItemState;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.presenters.ShoppingDetailsPresenter;
import com.filipwiniarz.shopapp.presenters.ShoppingListsActivityPresenter;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class AddProductDialog extends Dialog {

    private ShoppingDetailsPresenter presenter;
    private ShoppingList sourceList;

    @BindView(R.id.dialog_title) TextView dialogTitle;
    @BindView(R.id.name_edit_text) EditText productNameField;
    @BindView(R.id.amount_edit_text) EditText productAmountField;
    @BindView(R.id.dialog_save_button) Button saveButton;
    @BindView(R.id.dialog_cancel_button) Button cancelButton;

    public AddProductDialog(Context context, @NonNull ShoppingDetailsPresenter presenter, ShoppingList sourceList) {
        super(context);
        setContentView(R.layout.dialog_add_product);
        this.presenter = presenter;
        this.sourceList = sourceList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        saveButton.setEnabled(false);
        dialogTitle.setText(R.string.add_product);
    }

    @OnClick(R.id.dialog_cancel_button)
    public void cancelClick() {
        dismiss();
    }

    @OnClick(R.id.dialog_save_button)
    public void saveClick() {
        String name = productNameField.getText().toString();
        String amount = productAmountField.getText().toString();

        ShoppingItem newShoppingItem = new ShoppingItem();
        newShoppingItem.setName(name);
        newShoppingItem.setState(ShoppingItemState.ACTIVE);
        newShoppingItem.setCreateDate(new Date());
        newShoppingItem.setShoppingListId(sourceList.getId());
        newShoppingItem.setAmount(amount);

        dismiss();

        presenter.addListItem(newShoppingItem);
    }

    @OnTextChanged(R.id.name_edit_text)
    public void onTextChanged(CharSequence charSequence) {
        saveButton.setEnabled(charSequence.length() != 0);
    }
}
