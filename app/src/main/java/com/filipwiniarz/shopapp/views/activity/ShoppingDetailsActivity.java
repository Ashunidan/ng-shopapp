package com.filipwiniarz.shopapp.views.activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.presenters.ShoppingDetailsPresenter;
import com.filipwiniarz.shopapp.views.dialogs.AddProductDialog;
import com.filipwiniarz.shopapp.views.interfaces.ShoppingDetailsView;
import com.filipwiniarz.shopapp.views.listeners.ItemDeleteListener;
import com.filipwiniarz.shopapp.views.listeners.ItemMarkListener;
import com.filipwiniarz.shopapp.views.recycler.adapter.ShoppingItemsAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ShoppingDetailsActivity extends MvpActivity<ShoppingDetailsPresenter> implements ShoppingDetailsView, ItemDeleteListener<ShoppingItem>, ItemMarkListener {

    public static final String DETAILS_OBJECT_ARG = "details_object_arg";

    private ShoppingList shoppingList;
    private ShoppingItemsAdapter recyclerAdapter;
    @BindView(R.id.toolbar_layout) CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.detail_toolbar) Toolbar toolbar;
    @BindView(R.id.edit_title_fab) FloatingActionButton editTitleFab;
    @BindView(R.id.add_item_fab) FloatingActionButton addItemFab;
    @BindView(R.id.items_recycler) RecyclerView shoppingItemsRecycler;
    @BindView(R.id.empty_list_label) TextView emptyListLabel;
    @BindView(R.id.products_header) TextView productsTextView;

    @Override
    public ShoppingDetailsPresenter buildPresenter() {
        return new ShoppingDetailsPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        obtainBundleArgs();
        toolbarLayout.setTitle(shoppingList.getName());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if(shoppingList.getState() == ShoppingListState.ARCHIVED) {
            handleArchivedMode();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        prepareRecyclerView();
        getPresenter().fetchShoppingItemsForList(shoppingList.getId());
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_shopping_details;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            navigateUpTo(new Intent(this, ShoppingListsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void obtainBundleArgs() {
        if(getIntent().getExtras() != null) {
            shoppingList = (ShoppingList) getIntent().getExtras().getSerializable(DETAILS_OBJECT_ARG);
        } else {
            throw new IllegalStateException("Cannot start details activity without details argument!");
        }
    }

    private void prepareRecyclerView() {
        recyclerAdapter = new ShoppingItemsAdapter(this, this, (shoppingList.getState() == ShoppingListState.ARCHIVED));

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(shoppingItemsRecycler.getContext(), new LinearLayoutManager(this).getOrientation());

        shoppingItemsRecycler.setHasFixedSize(true);
        shoppingItemsRecycler.setLayoutManager(mLayoutManager);
        shoppingItemsRecycler.setItemAnimator(new DefaultItemAnimator());
        shoppingItemsRecycler.addItemDecoration(dividerItemDecoration);
        shoppingItemsRecycler.setAdapter(recyclerAdapter);
    }
    @Override
    public void displayShoppingItems(List<ShoppingItem> shoppingItems) {
        emptyListLabel.setVisibility(View.GONE);
        productsTextView.setVisibility(View.VISIBLE);
        shoppingItemsRecycler.setVisibility(View.VISIBLE);
        recyclerAdapter.setAndDisplayItems(shoppingItems);
    }

    @Override
    public void displayEmptyList() {
        emptyListLabel.setVisibility(View.VISIBLE);
        productsTextView.setVisibility(View.GONE);
        shoppingItemsRecycler.setVisibility(View.GONE);
    }

    @Override
    public void handleNewShoppingItem() {
        getPresenter().fetchShoppingItemsForList(shoppingList.getId());
        Snackbar.make(addItemFab, R.string.product_added, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    @Override
    public void handleRemoveShoppingItem(ShoppingItem item) {
        getPresenter().fetchShoppingItemsForList(shoppingList.getId());
        Snackbar.make(addItemFab, R.string.product_deleted, Snackbar.LENGTH_LONG).setAction(R.string.general_undo,
                v -> getPresenter().addListItem(item)).show();
    }

    @Override
    public void handleTitleEdit(ShoppingList list) {
        toolbarLayout.setTitle(list.getName());
    }

    @Override
    public void handleArchivedMode() {
        addItemFab.setVisibility(View.GONE);
        editTitleFab.setVisibility(View.GONE);
        toolbarLayout.setBackgroundColor(getResources().getColor(R.color.inactiveGray));
    }

    @Override
    public void onItemDelete(ShoppingItem item) {
        getPresenter().deleteListItem(item);
    }

    @Override
    public void onMark(ShoppingItem item, boolean marked) {
        getPresenter().markListItemAsBought(item, marked);
    }

    @OnClick(R.id.add_item_fab)
    public void onAddItemFabClick() {
        AddProductDialog dialog = new AddProductDialog(this, getPresenter(), shoppingList);
        dialog.show();
    }

    @OnClick(R.id.edit_title_fab)
    public void onEditTitleFabClick() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_add_shopping_list);
        ((TextView)dialog.findViewById(R.id.dialog_title)).setText(R.string.rename_shopping_list);
        ((EditText)dialog.findViewById(R.id.title_edit_text)).setText(shoppingList.getName());
        dialog.findViewById(R.id.dialog_cancel_button).setOnClickListener(v -> dialog.dismiss());
        dialog.findViewById(R.id.dialog_save_button).setOnClickListener(v -> {
                    getPresenter().updateListTitle(shoppingList, ((TextView)dialog.findViewById(R.id.title_edit_text)).getText().toString());
                    dialog.dismiss();
                });
        dialog.show();
    }
}
