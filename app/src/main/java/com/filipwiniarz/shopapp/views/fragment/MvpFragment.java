package com.filipwiniarz.shopapp.views.fragment;

import android.os.Bundle;
import android.view.View;

import com.filipwiniarz.shopapp.presenters.MvpPresenter;
import com.filipwiniarz.shopapp.views.MvpView;

public abstract class MvpFragment<T extends MvpPresenter> extends BaseFragment implements MvpView {

    private T presenter;

    public T getPresenter() {
        return presenter;
    }

    public abstract T buildPresenter();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = buildPresenter();
        presenter.attachView(this);
    }
}
