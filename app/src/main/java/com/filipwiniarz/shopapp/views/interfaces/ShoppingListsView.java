package com.filipwiniarz.shopapp.views.interfaces;

import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.views.MvpView;

import java.util.List;

public interface ShoppingListsView extends MvpView {

    void displayShoppingLists(List<ShoppingList> shoppingLists);

    void displayEmptyListMessage();

    void updateShoppingLists();

    void handleShoppingListArchive(ShoppingList shoppingList);

    void handleShoppingListUnarchive(ShoppingList shoppingList);

    void handleShoppingListDelete(ShoppingList shoppingList);
}
