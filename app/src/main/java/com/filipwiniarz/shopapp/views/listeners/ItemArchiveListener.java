package com.filipwiniarz.shopapp.views.listeners;

public interface ItemArchiveListener<T> {

    void onItemArchive(T item);
}
