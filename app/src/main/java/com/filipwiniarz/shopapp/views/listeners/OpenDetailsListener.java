package com.filipwiniarz.shopapp.views.listeners;

public interface OpenDetailsListener<T> {

    void onOpenDetails(T detailsItem);
}
