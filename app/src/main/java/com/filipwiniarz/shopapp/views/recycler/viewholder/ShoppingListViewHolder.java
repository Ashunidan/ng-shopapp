package com.filipwiniarz.shopapp.views.recycler.viewholder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.views.activity.ShoppingDetailsActivity;
import com.filipwiniarz.shopapp.views.listeners.OpenDetailsListener;
import com.filipwiniarz.shopapp.utils.DateUtils;
import com.filipwiniarz.shopapp.views.listeners.ItemArchiveListener;
import com.filipwiniarz.shopapp.views.listeners.ItemDeleteListener;
import com.filipwiniarz.shopapp.views.listeners.ItemUnarchiveListener;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.OnClick;

public class ShoppingListViewHolder extends BaseViewHolder<ShoppingList>{

    private final ItemDeleteListener<ShoppingList> deleteListener;
    private final ItemArchiveListener<ShoppingList> archiveListener;
    private final ItemUnarchiveListener<ShoppingList> unarchiveListener;
    private final OpenDetailsListener<ShoppingList> openDetailsListener;

    @BindView(R.id.root) View rootView;
    @BindView(R.id.shopping_list_title) TextView titleTextView;
    @BindView(R.id.shopping_list_date) TextView createDateTextView;
    @BindView(R.id.delete_button) ImageView deleteButton;
    @BindView(R.id.archive_button) ImageView archiveUnarchiveButton;

    public ShoppingListViewHolder(ViewGroup parent,
                                  ItemDeleteListener<ShoppingList> deleteListener,
                                  ItemArchiveListener<ShoppingList> archiveListener,
                                  ItemUnarchiveListener<ShoppingList> unarchiveListener,
                                  OpenDetailsListener<ShoppingList> openDetailsListener) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_shopping_list, parent, false));
        this.deleteListener = deleteListener;
        this.archiveListener = archiveListener;
        this.unarchiveListener = unarchiveListener;
        this.openDetailsListener = openDetailsListener;
        this.context = parent.getContext();
    }

    @Override
    public void bindItem(ShoppingList item) {
        this.item = item;
        rootView.setOnClickListener(v -> openDetailsListener.onOpenDetails(item));
        titleTextView.setText(item.getName());
        createDateTextView.setText(DateUtils.format(item.getCreateDate()));

        if(isArchiveButton()) {
            archiveUnarchiveButton.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_archive_black_24dp));
        } else if(isUnarchiveButton()) {
            archiveUnarchiveButton.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unarchive_black_24dp));
        }
    }

    @OnClick(R.id.archive_button)
    public void onArchiveOrUnarchive() {
        if(isArchiveButton()) {
            archiveListener.onItemArchive(item);
        } else if (isUnarchiveButton()) {
            unarchiveListener.onItemUnarchive(item);
        }
    }

    @OnClick(R.id.delete_button)
    public void onDelete() {
        if(deleteListener != null) {
            deleteListener.onItemDelete(item);
        }
    }

    private boolean isArchiveButton() {
        return archiveListener != null;
    }

    private boolean isUnarchiveButton() {
        return unarchiveListener != null;
    }
}
