package com.filipwiniarz.shopapp.views.listeners;

public interface ItemUnarchiveListener<T> {

    void onItemUnarchive(T item);
}
