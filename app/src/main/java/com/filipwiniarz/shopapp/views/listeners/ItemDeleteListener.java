package com.filipwiniarz.shopapp.views.listeners;


public interface ItemDeleteListener<T> {

    void onItemDelete(T item);
}
