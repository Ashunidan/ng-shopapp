package com.filipwiniarz.shopapp.views.interfaces;

import com.filipwiniarz.shopapp.views.MvpView;

public interface AddShoppingListView extends MvpView {

    void saveNewShoppingList();
    void cancel();
}
