package com.filipwiniarz.shopapp.views.recycler.adapter;

import android.view.ViewGroup;

import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.views.listeners.ItemDeleteListener;
import com.filipwiniarz.shopapp.views.listeners.ItemMarkListener;
import com.filipwiniarz.shopapp.views.recycler.viewholder.ShoppingItemViewHolder;

import java.util.ArrayList;

public class ShoppingItemsAdapter extends BaseRecyclerAdapter<ShoppingItem, ShoppingItemViewHolder> {

    private final ItemDeleteListener<ShoppingItem> deleteListener;
    private final ItemMarkListener markListener;
    private final boolean archived;

    public ShoppingItemsAdapter(ItemDeleteListener<ShoppingItem> deleteListener, ItemMarkListener markListener, boolean archived) {
        this.deleteListener = deleteListener;
        this.adapterItems = new ArrayList<>();
        this.markListener = markListener;
        this.archived = archived;
    }

    @Override
    public ShoppingItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShoppingItemViewHolder(parent, deleteListener, markListener, archived);
    }

    @Override
    public void onBindViewHolder(ShoppingItemViewHolder holder, int position) {
        holder.bindItem(adapterItems.get(position));
    }
}
