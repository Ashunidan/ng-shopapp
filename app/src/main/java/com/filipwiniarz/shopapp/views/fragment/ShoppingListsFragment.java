package com.filipwiniarz.shopapp.views.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.filipwiniarz.shopapp.R;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.presenters.ShoppingListsPresenter;
import com.filipwiniarz.shopapp.views.activity.ShoppingDetailsActivity;
import com.filipwiniarz.shopapp.views.activity.ShoppingListsActivity;
import com.filipwiniarz.shopapp.views.interfaces.ShoppingListsView;
import com.filipwiniarz.shopapp.views.listeners.ItemArchiveListener;
import com.filipwiniarz.shopapp.views.listeners.ItemDeleteListener;
import com.filipwiniarz.shopapp.views.listeners.ItemUnarchiveListener;
import com.filipwiniarz.shopapp.views.listeners.OpenDetailsListener;
import com.filipwiniarz.shopapp.views.recycler.adapter.ShoppingListsAdapter;

import java.util.List;

import butterknife.BindView;

public class ShoppingListsFragment extends MvpFragment<ShoppingListsPresenter>
        implements ShoppingListsView, ItemDeleteListener<ShoppingList>, ItemArchiveListener<ShoppingList>, ItemUnarchiveListener<ShoppingList>, OpenDetailsListener<ShoppingList> {

    private static final String SHOPPING_LIST_TYPE_ARG = "shopping_list_type_arg";

    @BindView(R.id.shopping_list_recycler) RecyclerView shoppingListRecycler;
    @BindView(R.id.empty_list_label) TextView emptyListTextView;

    private ShoppingListsAdapter recyclerAdapter;
    private String shoppingListsState;

    public static ShoppingListsFragment newInstance(@NonNull ShoppingListState shoppingListsType) {
        Bundle args = new Bundle();
        args.putString(SHOPPING_LIST_TYPE_ARG, shoppingListsType.name());

        ShoppingListsFragment fragment = new ShoppingListsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtainBundleArgs();
        prepareRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateShoppingLists();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_shopping_lists;
    }

    @Override
    public ShoppingListsPresenter buildPresenter() {
        return new ShoppingListsPresenter();
    }

    private void obtainBundleArgs() {
        Bundle args = getArguments();
        shoppingListsState = (args == null ? null : args.getString(SHOPPING_LIST_TYPE_ARG));
        recyclerAdapter = ShoppingListState.ACTIVE.name().equals(shoppingListsState) ? new ShoppingListsAdapter(this, this, null, this)
                                                                                     : new ShoppingListsAdapter(this, null, this, this);

        if(!ShoppingListState.ACTIVE.name().equals(shoppingListsState) && !ShoppingListState.ARCHIVED.name().equals(shoppingListsState)) {
            throw new IllegalArgumentException("Unsupported ShoppingListState [" + shoppingListsState + "]");
        }
    }

    private void prepareRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(shoppingListRecycler.getContext(), new LinearLayoutManager(getContext()).getOrientation());

        shoppingListRecycler.setHasFixedSize(true);
        shoppingListRecycler.setLayoutManager(mLayoutManager);
        shoppingListRecycler.setItemAnimator(new DefaultItemAnimator());
        shoppingListRecycler.addItemDecoration(dividerItemDecoration);
        shoppingListRecycler.setAdapter(recyclerAdapter);
    }

    @Override
    public void displayShoppingLists(List<ShoppingList> shoppingLists) {
        emptyListTextView.setVisibility(View.GONE);
        shoppingListRecycler.setVisibility(View.VISIBLE);
        recyclerAdapter.setAndDisplayItems(shoppingLists);
    }

    @Override
    public void displayEmptyListMessage() {
        int messageId = (shoppingListsState.equals(ShoppingListState.ACTIVE.name()) ? R.string.empty_list_active : R.string.empty_list_archived);
        emptyListTextView.setVisibility(View.VISIBLE);
        emptyListTextView.setText(messageId);
        shoppingListRecycler.setVisibility(View.GONE);
    }

    @Override
    public void updateShoppingLists() {
        getPresenter().fetchShoppingLists(ShoppingListState.valueOf(shoppingListsState));
    }

    @Override
    public void handleShoppingListArchive(ShoppingList shoppingList) {
        ((ShoppingListsActivity)getRootContext()).updateAllLists();
        View fab = ((Activity)getRootContext()).findViewById(R.id.add_shopping_list_fab);
        Snackbar.make(fab, R.string.shopping_list_archived, Snackbar.LENGTH_LONG).setAction(R.string.general_undo,
                v -> onItemUnarchive(shoppingList)).show();
    }

    @Override
    public void handleShoppingListUnarchive(ShoppingList shoppingList) {
        ((ShoppingListsActivity)getRootContext()).updateAllLists();
        View fab = ((Activity)getRootContext()).findViewById(R.id.add_shopping_list_fab);
        Snackbar.make(fab, R.string.shopping_list_unarchived, Snackbar.LENGTH_LONG).setAction(R.string.general_undo,
                v -> onItemArchive(shoppingList)).show();
    }

    @Override
    public void handleShoppingListDelete(ShoppingList shoppingList) {
        ((ShoppingListsActivity)getRootContext()).updateAllLists();
        View fab = ((Activity)getRootContext()).findViewById(R.id.add_shopping_list_fab);
        Snackbar.make(fab, R.string.shopping_list_deleted, Snackbar.LENGTH_LONG).setAction(R.string.general_undo,
                v -> ((ShoppingListsActivity)getRootContext()).getPresenter().restoreShoppingList(shoppingList)).show();
    }

    @Override
    public void onItemArchive(ShoppingList item) {
        getPresenter().archiveShoppingList(item);
    }

    @Override
    public void onItemDelete(ShoppingList item) {
        getPresenter().deleteShoppingList(item);
    }

    @Override
    public void onItemUnarchive(ShoppingList item) {
        getPresenter().unarchiveShoppingList(item);
    }

    @Override
    public void onOpenDetails(ShoppingList detailsItem) {
        Intent detailsIntent = new Intent(getRootContext(), ShoppingDetailsActivity.class);
        detailsIntent.putExtra(ShoppingDetailsActivity.DETAILS_OBJECT_ARG, detailsItem);
        getRootContext().startActivity(detailsIntent);
    }
}
