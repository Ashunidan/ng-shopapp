package com.filipwiniarz.shopapp.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static String format(Date date) {
        String dateString = null;

        if(date != null) {
            DateFormat df = SimpleDateFormat.getDateInstance();
            dateString = df.format(date);
        }

        return dateString;
    }
}
