package com.filipwiniarz.shopapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.filipwiniarz.shopapp.database.upgrade.DbUpgrader;

import java.util.HashMap;
import java.util.Map;

public class DbManager extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "shopapp_db";

    private final Map<Integer, DbUpgrader> upgraders;

    public DbManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        upgraders =  new HashMap<>();
        prepareUpgraders();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DbContract.ShoppingListTable.CREATE_TABLE);
        sqLiteDatabase.execSQL(DbContract.ShoppingItemTable.CREATE_TABLE);
        Log.d("DbManager", "OnCreate invoked");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        for(int i = oldVersion + 1; i <= newVersion; i++) {
            upgrade(sqLiteDatabase, i);
        }

        Log.d("DbManager", "OnUpgrade invoked");
    }

    private void upgrade(SQLiteDatabase database, int toVersion) {
        DbUpgrader upgrader = upgraders.get(toVersion);

        if(upgrader != null) {
            upgrader.onDbUpgrade(database);
        }
    }

    private void prepareUpgraders() {
        //empty, because no upgraders required yet
    }
}
