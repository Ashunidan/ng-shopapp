package com.filipwiniarz.shopapp.database.entities;

import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Entity class representing a shopping list.
 */
public class ShoppingList implements Serializable {
    private Long id;
    private String name;
    private Date createDate;
    private ShoppingListState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public ShoppingListState getState() {
        return state;
    }

    public void setState(ShoppingListState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingList that = (ShoppingList) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
