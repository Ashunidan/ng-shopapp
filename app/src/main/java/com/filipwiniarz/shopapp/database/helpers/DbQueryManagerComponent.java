package com.filipwiniarz.shopapp.database.helpers;

import dagger.Component;

@Component(modules = {DbQueryManagerModule.class})
public interface DbQueryManagerComponent {

    void inject(DbQueryManager queryManager);
}
