package com.filipwiniarz.shopapp.database.enums;

public enum ShoppingListState {
    ACTIVE,
    ARCHIVED,
    DELETED_ACTIVE,
    DELETED_ARCHIVED
}
