package com.filipwiniarz.shopapp.database;

import android.provider.BaseColumns;

public final class DbContract {

    private DbContract(){}

    public static class ShoppingListTable implements BaseColumns {
        public static final String TABLE_NAME = "shopping_list";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_CREATE_DATE = "create_date";
        public static final String COLUMN_STATE = "state";

        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_CREATE_DATE + " INTEGER, " +
                COLUMN_STATE + " TEXT" + ")";
    }

    public static class ShoppingItemTable implements BaseColumns {
        public static final String TABLE_NAME = "shopping_item";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_SHOPPING_LIST_ID = "shopping_list_id";
        public static final String COLUMN_CREATE_DATE = "create_date";
        public static final String COLUMN_STATE = "state";

        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_SHOPPING_LIST_ID + " INTEGER, " +
                COLUMN_AMOUNT + " TEXT, " +
                COLUMN_CREATE_DATE + " INTEGER, " +
                COLUMN_STATE + " TEXT" + ")";
    }
}
