package com.filipwiniarz.shopapp.database.enums;

public enum ShoppingItemState {
    ACTIVE,
    BOUGHT
}
