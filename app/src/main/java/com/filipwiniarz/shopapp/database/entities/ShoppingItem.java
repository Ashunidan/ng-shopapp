package com.filipwiniarz.shopapp.database.entities;

import com.filipwiniarz.shopapp.database.enums.ShoppingItemState;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Entity class representing Shopping list item (a single product-record on shopping list).
 */
public class ShoppingItem implements Serializable {
    private Long id;
    private String name;
    private String amount;
    private Long shoppingListId;
    private Date createDate;
    private ShoppingItemState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShoppingListId() {
        return shoppingListId;
    }

    public void setShoppingListId(Long shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public ShoppingItemState getState() {
        return state;
    }

    public void setState(ShoppingItemState state) {
        this.state = state;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShoppingItem)) return false;
        ShoppingItem that = (ShoppingItem) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {

        return id.hashCode();
    }
}
