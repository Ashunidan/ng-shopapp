package com.filipwiniarz.shopapp.database.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;
import com.filipwiniarz.shopapp.utils.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

public class ShoppingListDbServiceImpl extends BaseDbService implements ShoppingListDbService {

    @Override
    public ShoppingList fetchShoppingList(@NonNull Context context, @NonNull Long id) {
        List<ShoppingList> result;
        String tableName = DbContract.ShoppingListTable.TABLE_NAME;
        String idSelectionCriterion = DbContract.ShoppingListTable._ID + " = ?";
        String[] idSelectionArg = new String[]{id.toString()};

        Cursor cursor = null;
        try {
            cursor = getReadableDb(context).query(tableName,
                    getAllProjectionColumns(),
                    idSelectionCriterion,
                    idSelectionArg,
                    null,
                    null,
                    null);
            result = buildFromCursor(cursor);
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }

        return (CollectionUtils.isEmpty(result) ? null : result.get(0));
    }

    @Override
    public Flowable<List<ShoppingList>> fetchShoppingLists(@NonNull Context context, @Nullable ShoppingListState state) {
        Flowable<List<ShoppingList>> resultFlowable;
        String tableName = DbContract.ShoppingListTable.TABLE_NAME;

        String stateCriterion = (DbContract.ShoppingListTable.COLUMN_STATE + " IN (?, ?)");
        String[] args = (state == null ? new String[]{ShoppingListState.ACTIVE.name(), ShoppingListState.ARCHIVED.name()} : new String[]{state.name()});

        Cursor cursor = null;
        try {
            cursor = getReadableDb(context).query(tableName,
                    getAllProjectionColumns(),
                    stateCriterion,
                    args,
                    null,
                    null,
                    DbContract.ShoppingItemTable.COLUMN_CREATE_DATE + " DESC");
            resultFlowable = Flowable.fromArray(buildFromCursor(cursor));
        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }

        return resultFlowable;
    }

    @Override
    public Completable insertShoppingList(@NonNull Context context, @NonNull ShoppingList shoppingList) {
        return Completable.fromAction(() -> {
            String tableName = DbContract.ShoppingListTable.TABLE_NAME;

            ContentValues values = new ContentValues();
            values.put(DbContract.ShoppingListTable.COLUMN_NAME, shoppingList.getName());
            values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, shoppingList.getCreateDate().getTime());
            values.put(DbContract.ShoppingListTable.COLUMN_STATE, shoppingList.getState().name());

            getWritableDb(context).insert(tableName, null, values);
        });
    }

    @Override
    public Completable updateShoppingList(@NonNull Context context, @NonNull ShoppingList shoppingList) {
        return Completable.fromAction(() -> {
            String tableName = DbContract.ShoppingListTable.TABLE_NAME;
            String idSelectionCriterion = DbContract.ShoppingListTable._ID + " = ?";
            String[] idSelectionArg = new String[]{shoppingList.getId().toString()};

            ContentValues values = new ContentValues();
            values.put(DbContract.ShoppingListTable.COLUMN_NAME, shoppingList.getName());
            values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, shoppingList.getCreateDate().getTime());
            values.put(DbContract.ShoppingListTable.COLUMN_STATE, shoppingList.getState().name());

            getWritableDb(context).update(tableName, values, idSelectionCriterion, idSelectionArg);
        });
    }

    private String[] getAllProjectionColumns() {
        return new String[]{DbContract.ShoppingListTable._ID,
                            DbContract.ShoppingListTable.COLUMN_NAME,
                            DbContract.ShoppingListTable.COLUMN_CREATE_DATE,
                            DbContract.ShoppingListTable.COLUMN_STATE};
    }

    private List<ShoppingList> buildFromCursor(Cursor cursor) {
        List<ShoppingList> resultList = new ArrayList<>();

        if(cursor != null) {
            while (cursor.moveToNext()) {
                ShoppingList result = new ShoppingList();

                Long id = cursor.getLong(cursor.getColumnIndex(DbContract.ShoppingListTable._ID));
                String name = cursor.getString(cursor.getColumnIndex(DbContract.ShoppingListTable.COLUMN_NAME));
                Date createDate = new Date(cursor.getLong(cursor.getColumnIndex(DbContract.ShoppingListTable.COLUMN_CREATE_DATE)));
                ShoppingListState state = ShoppingListState.valueOf(cursor.getString(cursor.getColumnIndex(DbContract.ShoppingListTable.COLUMN_STATE)));

                result.setId(id);
                result.setName(name);
                result.setCreateDate(createDate);
                result.setState(state);
                resultList.add(result);
            }
        }

        return resultList;
    }
}
