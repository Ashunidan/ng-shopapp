package com.filipwiniarz.shopapp.database.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.enums.ShoppingItemState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class ShoppingItemDbServiceImpl extends BaseDbService implements ShoppingItemDbService {

    @Override
    public Flowable<List<ShoppingItem>> fetchShoppingItemsForList(@NonNull Context context, Long shoppingListId) {
        Flowable<List<ShoppingItem>> resultFlowable;
        String tableName = DbContract.ShoppingItemTable.TABLE_NAME;
        String listIdCriterion = (shoppingListId == null ? null : DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID + " = ?");
        String[] listIdArg = (shoppingListId == null ? null : new String[]{shoppingListId.toString()});

        Cursor cursor = null;
        try {
            cursor = getReadableDb(context).query(tableName,
                    getAllProjectionColumns(),
                    listIdCriterion,
                    listIdArg,
                    null,
                    null,
                    DbContract.ShoppingItemTable.COLUMN_CREATE_DATE + " DESC");
            resultFlowable = Flowable.fromArray(buildFromCursor(cursor));

        } finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        
        return resultFlowable;
    }

    @Override
    public Completable insertShoppingItem(@NonNull Context context, @NonNull ShoppingItem shoppingItem) {
        return Completable.fromAction(() -> {
            String tableName = DbContract.ShoppingItemTable.TABLE_NAME;

            ContentValues values = new ContentValues();
            values.put(DbContract.ShoppingItemTable.COLUMN_NAME, shoppingItem.getName());
            values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, shoppingItem.getCreateDate().getTime());
            values.put(DbContract.ShoppingItemTable.COLUMN_STATE, shoppingItem.getState().name());
            values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, shoppingItem.getAmount());
            values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, shoppingItem.getShoppingListId());

            getWritableDb(context).insert(tableName, null, values);
        });
    }

    @Override
    public Completable updateShoppingItem(@NonNull Context context, @NonNull ShoppingItem shoppingItem) {
        return Completable.fromAction(() -> {
            String tableName = DbContract.ShoppingItemTable.TABLE_NAME;
            String idSelectionCriterion = DbContract.ShoppingItemTable._ID + " = ?";
            String[] idSelectionArg = new String[]{shoppingItem.getId().toString()};

            ContentValues values = new ContentValues();
            values.put(DbContract.ShoppingItemTable.COLUMN_NAME, shoppingItem.getName());
            values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, shoppingItem.getCreateDate().getTime());
            values.put(DbContract.ShoppingItemTable.COLUMN_STATE, shoppingItem.getState().name());
            values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, shoppingItem.getAmount());
            values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, shoppingItem.getShoppingListId());

            getWritableDb(context).update(tableName, values, idSelectionCriterion, idSelectionArg);
        });
    }

    @Override
    public Completable deleteShoppingItem(@NonNull Context context, @NonNull Long id) {
        return Completable.fromAction(() -> {
            String tableName = DbContract.ShoppingItemTable.TABLE_NAME;
            String idSelectionCriterion = DbContract.ShoppingItemTable._ID + " = ?";
            String[] idSelectionArg = new String[]{id.toString()};

            getWritableDb(context).delete(tableName, idSelectionCriterion, idSelectionArg);
        });
    }

    private String[] getAllProjectionColumns() {
        return new String[]{DbContract.ShoppingItemTable._ID,
                DbContract.ShoppingItemTable.COLUMN_NAME,
                DbContract.ShoppingItemTable.COLUMN_AMOUNT,
                DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID,
                DbContract.ShoppingItemTable.COLUMN_CREATE_DATE,
                DbContract.ShoppingItemTable.COLUMN_STATE};
    }

    private List<ShoppingItem> buildFromCursor(Cursor cursor) {
        List<ShoppingItem> resultList = new ArrayList<>();

        if(cursor != null) {
            while (cursor.moveToNext()) {
                ShoppingItem result = new ShoppingItem();

                Long id = cursor.getLong(cursor.getColumnIndex(DbContract.ShoppingItemTable._ID));
                String name = cursor.getString(cursor.getColumnIndex(DbContract.ShoppingItemTable.COLUMN_NAME));
                String amount = cursor.getString(cursor.getColumnIndex(DbContract.ShoppingItemTable.COLUMN_AMOUNT));
                Long shoppingListId = cursor.getLong(cursor.getColumnIndex(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID));
                Date createDate = new Date(cursor.getLong(cursor.getColumnIndex(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE)));
                ShoppingItemState state = ShoppingItemState.valueOf(cursor.getString(cursor.getColumnIndex(DbContract.ShoppingItemTable.COLUMN_STATE)));

                result.setId(id);
                result.setName(name);
                result.setAmount(amount);
                result.setShoppingListId(shoppingListId);
                result.setCreateDate(createDate);
                result.setState(state);
                resultList.add(result);
            }
        }

        return resultList;
    }
}
