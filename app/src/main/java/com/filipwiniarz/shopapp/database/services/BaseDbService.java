package com.filipwiniarz.shopapp.database.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.filipwiniarz.shopapp.app.App;

abstract class BaseDbService {

    SQLiteDatabase getWritableDb(@NonNull Context context) {
        return ((App)context.getApplicationContext()).getDatabase().getWritableDatabase();
    }

    SQLiteDatabase getReadableDb(@NonNull Context context) {
        return ((App)context.getApplicationContext()).getDatabase().getReadableDatabase();
    }
}
