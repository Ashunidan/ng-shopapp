package com.filipwiniarz.shopapp.database.upgrade;

import android.database.sqlite.SQLiteDatabase;

public interface DbUpgrader {

    void onDbUpgrade(SQLiteDatabase db);
}
