package com.filipwiniarz.shopapp.database.services;

import android.content.Context;
import android.support.annotation.NonNull;

import com.filipwiniarz.shopapp.database.entities.ShoppingItem;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface ShoppingItemDbService {

    Flowable<List<ShoppingItem>> fetchShoppingItemsForList(@NonNull Context context, Long shoppingListId);
    Completable insertShoppingItem(@NonNull Context context, @NonNull ShoppingItem shoppingItem);
    Completable updateShoppingItem(@NonNull Context context, @NonNull ShoppingItem shoppingItem);
    Completable deleteShoppingItem(@NonNull Context context, @NonNull Long id);
}
