package com.filipwiniarz.shopapp.database.helpers;

import com.filipwiniarz.shopapp.database.services.ShoppingItemDbService;
import com.filipwiniarz.shopapp.database.services.ShoppingItemDbServiceImpl;
import com.filipwiniarz.shopapp.database.services.ShoppingListDbService;
import com.filipwiniarz.shopapp.database.services.ShoppingListDbServiceImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class DbQueryManagerModule {
    private final DbQueryManager queryManager;

    public DbQueryManagerModule(DbQueryManager queryManager) {
        this.queryManager = queryManager;
    }

    @Provides
    ShoppingListDbService provideShoppingListDbService() {
        return new ShoppingListDbServiceImpl();
    }

    @Provides
    ShoppingItemDbService provideShoppingItemDbService() {
        return new ShoppingItemDbServiceImpl();
    }
}
