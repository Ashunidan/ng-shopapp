package com.filipwiniarz.shopapp.database.helpers;

import com.filipwiniarz.shopapp.database.services.ShoppingItemDbService;
import com.filipwiniarz.shopapp.database.services.ShoppingListDbService;

import javax.inject.Inject;

public class DbQueryManager {

    @Inject ShoppingListDbService shoppingListDbService;
    @Inject ShoppingItemDbService shoppingItemDbService;
    private DbQueryManagerComponent mComponent;

    private DbQueryManager() {
        if(mComponent == null) {
            mComponent = DaggerDbQueryManagerComponent.builder()
                    .dbQueryManagerModule(new DbQueryManagerModule(this))
                    .build();
        }

        mComponent.inject(this);
    }

    public static DbQueryManager createInstance() {
        return new DbQueryManager();
    }

    public ShoppingListDbService getShoppingListDbService() {
        return shoppingListDbService;
    }

    public ShoppingItemDbService getShoppingItemDbService() {
        return shoppingItemDbService;
    }
}
