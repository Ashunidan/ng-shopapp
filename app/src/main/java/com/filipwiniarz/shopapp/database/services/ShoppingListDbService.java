package com.filipwiniarz.shopapp.database.services;

import android.content.Context;
import android.support.annotation.NonNull;

import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface ShoppingListDbService {

    ShoppingList fetchShoppingList(@NonNull Context context, @NonNull Long id);
    Flowable<List<ShoppingList>> fetchShoppingLists(@NonNull Context context, ShoppingListState stateCriterion);
    Completable insertShoppingList(@NonNull Context context, @NonNull ShoppingList shoppingList);
    Completable updateShoppingList(@NonNull Context context, @NonNull ShoppingList shoppingList);
}
