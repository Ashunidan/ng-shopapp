package com.filipwiniarz.shopapp;

import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingItemState;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;

import java.util.Date;

public class Mocks {

    public static ShoppingList activeList1() {
        ShoppingList list = new ShoppingList();
        list.setName("MockActiveList1");
        list.setCreateDate(new Date(1494166069));
        list.setState(ShoppingListState.ACTIVE);
        return list;
    }

    public static ShoppingList activeList2() {
        ShoppingList list = new ShoppingList();
        list.setName("Another-list");
        list.setCreateDate(new Date(1520160));
        list.setState(ShoppingListState.ACTIVE);
        return list;
    }

    public static ShoppingList activeList3() {
        ShoppingList list = new ShoppingList();
        list.setName("Some_random_name");
        list.setCreateDate(new Date(1524166069));
        list.setState(ShoppingListState.ACTIVE);
        return list;
    }

    public static ShoppingList archivedList1() {
        ShoppingList list = new ShoppingList();
        list.setName("Buy something today");
        list.setCreateDate(new Date(120000000));
        list.setState(ShoppingListState.ARCHIVED);
        return list;
    }

    public static ShoppingList archivedList2() {
        ShoppingList list = new ShoppingList();
        list.setName("Already bought");
        list.setCreateDate(new Date(140000000));
        list.setState(ShoppingListState.ARCHIVED);
        return list;
    }

    public static ShoppingList archivedList3() {
        ShoppingList list = new ShoppingList();
        list.setName("Long forgotten list");
        list.setCreateDate(new Date(150000000));
        list.setState(ShoppingListState.ARCHIVED);
        return list;
    }

    public static ShoppingList deletedActiveList1() {
        ShoppingList list = new ShoppingList();
        list.setName("Deleted as active");
        list.setCreateDate(new Date(1510000000));
        list.setState(ShoppingListState.DELETED_ACTIVE);
        return list;
    }

    public static ShoppingList deletedArchivedList1() {
        ShoppingList list = new ShoppingList();
        list.setName("Archived then deleted");
        list.setCreateDate(new Date(1250000000));
        list.setState(ShoppingListState.DELETED_ARCHIVED);
        return list;
    }

    public static ShoppingItem item1() {
        ShoppingItem item = new ShoppingItem();
        item.setName("Name1");
        item.setState(ShoppingItemState.ACTIVE);
        item.setAmount("150 g");
        item.setShoppingListId(1L);
        item.setCreateDate(new Date(123123123));
        return item;
    }

    public static ShoppingItem item2() {
        ShoppingItem item = new ShoppingItem();
        item.setName("Name2");
        item.setState(ShoppingItemState.BOUGHT);
        item.setAmount("8");
        item.setShoppingListId(1L);
        item.setCreateDate(new Date(1451451451));
        return item;
    }

    public static ShoppingItem item3() {
        ShoppingItem item = new ShoppingItem();
        item.setName("Name3");
        item.setState(ShoppingItemState.ACTIVE);
        item.setAmount("0,5 l");
        item.setShoppingListId(1L);
        item.setCreateDate(new Date(122222222));
        return item;
    }

    public static ShoppingItem item4() {
        ShoppingItem item = new ShoppingItem();
        item.setName("Name4");
        item.setState(ShoppingItemState.BOUGHT);
        item.setAmount("150 g");
        item.setShoppingListId(2L);
        item.setCreateDate(new Date(150));
        return item;
    }

    public static ShoppingItem item5() {
        ShoppingItem item = new ShoppingItem();
        item.setName("Name5");
        item.setState(ShoppingItemState.ACTIVE);
        item.setAmount("15");
        item.setShoppingListId(2L);
        item.setCreateDate(new Date(15002));
        return item;
    }

    public static ShoppingItem item6() {
        ShoppingItem item = new ShoppingItem();
        item.setName("Name6");
        item.setState(ShoppingItemState.BOUGHT);
        item.setAmount("154");
        item.setShoppingListId(3L);
        item.setCreateDate(new Date(1333333330));
        return item;
    }
}
