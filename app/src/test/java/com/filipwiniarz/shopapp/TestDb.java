package com.filipwiniarz.shopapp;

import android.content.ContentValues;

import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.DbManager;
import com.filipwiniarz.shopapp.database.entities.ShoppingItem;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;

public class TestDb {

    public void insertActiveLists(DbManager dbManager) {
        ShoppingList list1 = Mocks.activeList1();
        ShoppingList list2 = Mocks.activeList2();
        ShoppingList list3 = Mocks.activeList3();

        ContentValues values = new ContentValues();
        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list1.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list1.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list1.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list2.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list2.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list2.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list3.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list3.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list3.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);
    }

    public void insertArchivedLists(DbManager dbManager) {
        ShoppingList list1 = Mocks.archivedList1();
        ShoppingList list2 = Mocks.archivedList2();
        ShoppingList list3 = Mocks.archivedList3();

        ContentValues values = new ContentValues();
        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list1.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list1.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list1.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list2.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list2.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list2.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list3.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list3.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list3.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);
    }

    public void insertDeletedActiveLists(DbManager dbManager) {
        ShoppingList list1 = Mocks.deletedActiveList1();

        ContentValues values = new ContentValues();
        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list1.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list1.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list1.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);
    }

    public void insertDeletedArchivedLists(DbManager dbManager) {
        ShoppingList list1 = Mocks.deletedActiveList1();

        ContentValues values = new ContentValues();
        values.put(DbContract.ShoppingListTable.COLUMN_NAME, list1.getName());
        values.put(DbContract.ShoppingListTable.COLUMN_CREATE_DATE, list1.getCreateDate().getTime());
        values.put(DbContract.ShoppingListTable.COLUMN_STATE, list1.getState().name());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingListTable.TABLE_NAME, null, values);
    }

    public void insertAllItems(DbManager dbManager) {
        ShoppingItem item1 = Mocks.item1();
        ShoppingItem item2 = Mocks.item2();
        ShoppingItem item3 = Mocks.item3();
        ShoppingItem item4 = Mocks.item4();
        ShoppingItem item5 = Mocks.item5();
        ShoppingItem item6 = Mocks.item6();
        ContentValues values = new ContentValues();

        values.put(DbContract.ShoppingItemTable.COLUMN_NAME, item1.getName());
        values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, item1.getCreateDate().getTime());
        values.put(DbContract.ShoppingItemTable.COLUMN_STATE, item1.getState().name());
        values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, item1.getAmount());
        values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, item1.getShoppingListId());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingItemTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingItemTable.COLUMN_NAME, item2.getName());
        values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, item2.getCreateDate().getTime());
        values.put(DbContract.ShoppingItemTable.COLUMN_STATE, item2.getState().name());
        values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, item2.getAmount());
        values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, item2.getShoppingListId());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingItemTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingItemTable.COLUMN_NAME, item3.getName());
        values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, item3.getCreateDate().getTime());
        values.put(DbContract.ShoppingItemTable.COLUMN_STATE, item3.getState().name());
        values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, item3.getAmount());
        values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, item3.getShoppingListId());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingItemTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingItemTable.COLUMN_NAME, item4.getName());
        values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, item4.getCreateDate().getTime());
        values.put(DbContract.ShoppingItemTable.COLUMN_STATE, item4.getState().name());
        values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, item4.getAmount());
        values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, item4.getShoppingListId());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingItemTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingItemTable.COLUMN_NAME, item5.getName());
        values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, item5.getCreateDate().getTime());
        values.put(DbContract.ShoppingItemTable.COLUMN_STATE, item5.getState().name());
        values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, item5.getAmount());
        values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, item5.getShoppingListId());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingItemTable.TABLE_NAME, null, values);

        values.put(DbContract.ShoppingItemTable.COLUMN_NAME, item6.getName());
        values.put(DbContract.ShoppingItemTable.COLUMN_CREATE_DATE, item6.getCreateDate().getTime());
        values.put(DbContract.ShoppingItemTable.COLUMN_STATE, item6.getState().name());
        values.put(DbContract.ShoppingItemTable.COLUMN_AMOUNT, item6.getAmount());
        values.put(DbContract.ShoppingItemTable.COLUMN_SHOPPING_LIST_ID, item6.getShoppingListId());
        dbManager.getWritableDatabase().insert(DbContract.ShoppingItemTable.TABLE_NAME, null, values);
    }
}
