package com.filipwiniarz.shopapp.database.services;

import com.filipwiniarz.shopapp.BuildConfig;
import com.filipwiniarz.shopapp.Mocks;
import com.filipwiniarz.shopapp.TestDb;
import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.DbManager;
import com.filipwiniarz.shopapp.database.entities.ShoppingItem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import io.reactivex.functions.Action;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = LOLLIPOP, packageName = "com.filipwiniarz.shopapp")
public class ShoppingItemDbServiceImplTest {
    private DbManager dbManager;
    private ShoppingItemDbServiceImpl dbService = new ShoppingItemDbServiceImpl();
    private TestDb testDb = new TestDb();

    @Before
    public void setUp() {
        dbManager = new DbManager(RuntimeEnvironment.application);
    }

    @After
    public void tearDown() {
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingListTable.TABLE_NAME);
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingItemTable.TABLE_NAME);
    }

    @Test
    public void fetchShoppingItems_oneItemExpected() {
        testDb.insertAllItems(dbManager);
        TestSubscriber<List<ShoppingItem>> ts = new TestSubscriber<>();
        dbService.fetchShoppingItemsForList(RuntimeEnvironment.application, 3L).subscribe(ts);

        ShoppingItem expectedItem = Mocks.item6();
        ShoppingItem item = ts.values().get(0).get(0);

        assertEquals(ts.values().get(0).size(), 1);
        assertEquals(item.getName(), expectedItem.getName());
        assertEquals(item.getCreateDate(), expectedItem.getCreateDate());
        assertEquals(item.getState(), expectedItem.getState());
        assertEquals(item.getAmount(), expectedItem.getAmount());
        assertEquals(item.getShoppingListId(), expectedItem.getShoppingListId());
    }

    @Test
    public void insertShoppingItem_subscribedAndCompletedExpected() {
        TestObserver<Action> to = new TestObserver<>();
        dbService.insertShoppingItem(RuntimeEnvironment.application, Mocks.item3()).subscribe(to);

        to.assertSubscribed();
        to.assertNoErrors();
        to.assertComplete();
    }

    @Test
    public void insertShoppingItem_oneInsertedItemExpected() {
        TestObserver<Action> to = new TestObserver<>();
        TestSubscriber<List<ShoppingItem>> ts = new TestSubscriber<>();
        dbService.insertShoppingItem(RuntimeEnvironment.application, Mocks.item2()).subscribe(to);
        dbService.fetchShoppingItemsForList(RuntimeEnvironment.application, 1L).subscribe(ts);

        List<ShoppingItem> items = ts.values().get(0);
        ShoppingItem expectedItem = Mocks.item2();
        expectedItem.setId(1L);

        assertTrue(items.size() == 1);
        assertEquals(items.get(0), expectedItem);
    }

    @Test
    public void updateShoppingList_subscribedAndCompletedExpected() {
        testDb.insertAllItems(dbManager);
        TestObserver<Action> to = new TestObserver<>();

        ShoppingItem itemToUpdate = Mocks.item5();
        itemToUpdate.setId(5L);
        dbService.updateShoppingItem(RuntimeEnvironment.application, itemToUpdate).subscribe(to);

        to.assertSubscribed();
        to.assertNoErrors();
        to.assertComplete();
    }

    @Test
    public void updateShoppingList_oneUpdatedItemExpected() {
        testDb.insertAllItems(dbManager);
        TestObserver<Action> to = new TestObserver<>();
        TestSubscriber<List<ShoppingItem>> ts = new TestSubscriber<>();

        ShoppingItem itemToUpdate = Mocks.item1();
        itemToUpdate.setId(1L);
        dbService.updateShoppingItem(RuntimeEnvironment.application, itemToUpdate).subscribe(to);
        dbService.fetchShoppingItemsForList(RuntimeEnvironment.application, 1L).subscribe(ts);

        List<ShoppingItem> items = ts.values().get(0);

        assertEquals(items.get(1).getName(), itemToUpdate.getName());
        assertEquals(items.get(1).getShoppingListId(), itemToUpdate.getShoppingListId());
        assertEquals(items.get(1).getAmount(), itemToUpdate.getAmount());
        assertEquals(items.get(1).getCreateDate(), itemToUpdate.getCreateDate());
        assertEquals(items.get(1).getState(), itemToUpdate.getState());
    }
}