package com.filipwiniarz.shopapp.database.services;


import com.filipwiniarz.shopapp.BuildConfig;
import com.filipwiniarz.shopapp.Mocks;
import com.filipwiniarz.shopapp.TestDb;
import com.filipwiniarz.shopapp.database.DbContract;
import com.filipwiniarz.shopapp.database.DbManager;
import com.filipwiniarz.shopapp.database.entities.ShoppingList;
import com.filipwiniarz.shopapp.database.enums.ShoppingListState;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import io.reactivex.functions.Action;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = LOLLIPOP, packageName = "com.filipwiniarz.shopapp")
public class ShoppingListDbServiceImplTest {

    private DbManager dbManager;
    private ShoppingListDbServiceImpl dbService = new ShoppingListDbServiceImpl();
    private TestDb testDb = new TestDb();

    @Before
    public void setUp() {
        dbManager = new DbManager(RuntimeEnvironment.application);
    }

    @After
    public void tearDown() {
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingListTable.TABLE_NAME);
        dbManager.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DbContract.ShoppingItemTable.TABLE_NAME);
    }

    @Test
    public void fetchShoppingList_checkPersistedFields() {
        testDb.insertActiveLists(dbManager);
        ShoppingList list = dbService.fetchShoppingList(RuntimeEnvironment.application, 1L);
        ShoppingList expectedList = Mocks.activeList1();
        expectedList.setId(1L);

        assertEquals(list.getId(), expectedList.getId());
        assertEquals(list.getName(), expectedList.getName());
        assertEquals(list.getCreateDate(), expectedList.getCreateDate());
        assertEquals(list.getState(), expectedList.getState());
    }

    @Test
    public void fetchShoppingLists_threeActiveOrderedListsExpected() {
        testDb.insertActiveLists(dbManager);
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.fetchShoppingLists(RuntimeEnvironment.application, ShoppingListState.ACTIVE).subscribe(ts);

        List<ShoppingList> lists = ts.values().get(0);

        assertEquals(lists.size(), 3);
        assertEquals(lists.get(0).getCreateDate(), (Mocks.activeList3().getCreateDate()));
        assertEquals(lists.get(1).getCreateDate(), (Mocks.activeList1().getCreateDate()));
        assertEquals(lists.get(2).getCreateDate(), (Mocks.activeList2().getCreateDate()));
        ts.assertSubscribed();
        ts.assertNoErrors();
        ts.assertValueCount(1);
    }

    @Test
    public void fetchShoppingLists_zeroArchivedListsExpected() {
        testDb.insertActiveLists(dbManager);
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.fetchShoppingLists(RuntimeEnvironment.application, ShoppingListState.ARCHIVED).subscribe(ts);

        ts.assertSubscribed();
        ts.assertNoErrors();
        ts.assertValueCount(1);
        assertTrue(ts.values().get(0).isEmpty());
    }

    @Test
    public void fetchShoppingLists_threeArchivedOrderedListsExpected() {
        testDb.insertArchivedLists(dbManager);
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.fetchShoppingLists(RuntimeEnvironment.application, ShoppingListState.ARCHIVED).subscribe(ts);

        List<ShoppingList> lists = ts.values().get(0);

        assertEquals(lists.size(), 3);
        assertEquals(lists.get(0).getCreateDate(), (Mocks.archivedList3().getCreateDate()));
        assertEquals(lists.get(1).getCreateDate(), (Mocks.archivedList2().getCreateDate()));
        assertEquals(lists.get(2).getCreateDate(), (Mocks.archivedList1().getCreateDate()));
        ts.assertSubscribed();
        ts.assertNoErrors();
        ts.assertValueCount(1);
    }

    @Test
    public void fetchShoppingLists_zeroActiveListsExpected() {
        testDb.insertArchivedLists(dbManager);
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.fetchShoppingLists(RuntimeEnvironment.application, ShoppingListState.ACTIVE).subscribe(ts);

        ts.assertSubscribed();
        ts.assertNoErrors();
        ts.assertValueCount(1);
        assertTrue(ts.values().get(0).isEmpty());
    }

    @Test
    public void fetchShoppingLists_sixOrderedListsExpected() {
        testDb.insertActiveLists(dbManager);
        testDb.insertArchivedLists(dbManager);
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.fetchShoppingLists(RuntimeEnvironment.application, null).subscribe(ts);

        List<ShoppingList> lists = ts.values().get(0);

        assertEquals(lists.size(), 6);
        assertEquals(lists.get(0).getCreateDate(), (Mocks.activeList3().getCreateDate()));
        assertEquals(lists.get(1).getCreateDate(), (Mocks.activeList1().getCreateDate()));
        assertEquals(lists.get(2).getCreateDate(), (Mocks.archivedList3().getCreateDate()));
        assertEquals(lists.get(3).getCreateDate(), (Mocks.archivedList2().getCreateDate()));
        assertEquals(lists.get(4).getCreateDate(), (Mocks.archivedList1().getCreateDate()));
        assertEquals(lists.get(5).getCreateDate(), (Mocks.activeList2().getCreateDate()));
        ts.assertSubscribed();
        ts.assertNoErrors();
        ts.assertValueCount(1);
    }

    @Test
    public void fetchShoppingLists_zeroListsExpected() {
        testDb.insertDeletedActiveLists(dbManager);
        testDb.insertDeletedArchivedLists(dbManager);
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.fetchShoppingLists(RuntimeEnvironment.application, null).subscribe(ts);

        ts.assertSubscribed();
        ts.assertNoErrors();
        ts.assertValueCount(1);
        assertTrue(ts.values().get(0).isEmpty());
    }

    @Test
    public void insertShoppingList_subscribedAndCompletedExpected() {
        TestObserver<Action> to = new TestObserver<>();
        dbService.insertShoppingList(RuntimeEnvironment.application, Mocks.activeList2()).subscribe(to);

        to.assertSubscribed();
        to.assertNoErrors();
        to.assertComplete();
    }

    @Test
    public void insertShoppingList_oneInsertedItemExpected() {
        TestObserver<Action> to = new TestObserver<>();
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();
        dbService.insertShoppingList(RuntimeEnvironment.application, Mocks.activeList2()).subscribe(to);
        dbService.fetchShoppingLists(RuntimeEnvironment.application, null).subscribe(ts);

        List<ShoppingList> lists = ts.values().get(0);
        ShoppingList expectedList = Mocks.activeList2();
        expectedList.setId(1L);

        assertTrue(lists.size() == 1);
        assertEquals(lists.get(0), expectedList);
    }

    @Test
    public void updateShoppingList_subscribedAndCompletedExpected() {
        testDb.insertActiveLists(dbManager);
        TestObserver<Action> to = new TestObserver<>();

        ShoppingList listToUpdate = Mocks.activeList2();
        listToUpdate.setId(2L);
        dbService.updateShoppingList(RuntimeEnvironment.application, listToUpdate).subscribe(to);

        to.assertSubscribed();
        to.assertNoErrors();
        to.assertComplete();
    }

    @Test
    public void updateShoppingList_oneUpdatedItemExpected() {
        testDb.insertDeletedActiveLists(dbManager);
        TestObserver<Action> to = new TestObserver<>();
        TestSubscriber<List<ShoppingList>> ts = new TestSubscriber<>();

        ShoppingList listToUpdate = Mocks.activeList2();
        listToUpdate.setId(1L);
        dbService.updateShoppingList(RuntimeEnvironment.application, listToUpdate).subscribe(to);
        dbService.fetchShoppingLists(RuntimeEnvironment.application, null).subscribe(ts);

        List<ShoppingList> lists = ts.values().get(0);

        assertTrue(lists.size() == 1);
        assertEquals(lists.get(0).getName(), listToUpdate.getName());
        assertEquals(lists.get(0).getCreateDate(), listToUpdate.getCreateDate());
        assertEquals(lists.get(0).getState(), listToUpdate.getState());
    }
}