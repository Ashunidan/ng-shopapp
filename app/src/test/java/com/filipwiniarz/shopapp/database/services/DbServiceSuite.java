package com.filipwiniarz.shopapp.database.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ShoppingListDbServiceImplTest.class,
        ShoppingItemDbServiceImplTest.class
})
public class DbServiceSuite {
}
